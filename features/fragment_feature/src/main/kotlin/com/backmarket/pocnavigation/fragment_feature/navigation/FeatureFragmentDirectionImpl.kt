package com.backmarket.pocnavigation.fragment_feature.navigation

import com.backmarket.pocnavigation.fragment_feature_api.FeatureFragmentDirection
import com.backmarket.pocnavigation.fragment_feature.FeatureFragment
import com.backmarket.pocnavigation.navigation.direction.NavDirectionContext

class FeatureFragmentDirectionImpl(
    params: FeatureFragmentDirection.Params
) : FeatureFragmentDirection {
    override val navContext = NavDirectionContext(extra = params)
    override fun toScreen() = FeatureFragment.newInstance()
}
