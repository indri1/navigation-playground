listOf(
        ":app",

        ":features:home",

        ":features:activity_feature",
        ":features:activity_feature_api",

        ":features:fragment_feature",
        ":features:fragment_feature_api",

        ":navigation",
        ":design",
        ":core"
).forEach {
    include(it)
}
