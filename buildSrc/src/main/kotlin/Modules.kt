/**
 * Lists all the internal modules used in the project.
 */
object Modules {
    const val Home = ":features:home"

    const val ActivityFeature = ":features:activity_feature"
    const val ActivityFeatureApi = ":features:activity_feature_api"

    const val FragmentFeature = ":features:fragment_feature"
    const val FragmentFeatureApi = ":features:fragment_feature_api"

    const val Navigation = ":navigation"
    const val Design = ":design"
    const val Core = ":core"
}
